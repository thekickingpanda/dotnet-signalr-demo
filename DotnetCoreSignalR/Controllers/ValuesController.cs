﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotnetCoreSignalR.Hubs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace DotnetCoreSignalR.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        public static List<string> Source { get; set; } = new List<string>();

        private IHubContext<MessageHub> context;

        public ValuesController(IHubContext<MessageHub> hub)
        {
            this.context = hub;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return Source;
        }

        // GET api/values/skldhf
        [HttpGet("{value}")]
        public async Task<string> Get(string value)
        {
            Source.Add(value);
            await context.Clients.All.SendAsync("Add", value);
            return "ok";
        }

        // POST api/values
        [HttpPost]
        public async void Post([FromBody] string value)
        {
            Source.Add(value);
            await context.Clients.All.SendAsync("Add", value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async  void Delete(int id)
        {
            var item = Source[id];
            Source.Remove(item);
            await context.Clients.All.SendAsync("Delete", item);
        }
    }
}
