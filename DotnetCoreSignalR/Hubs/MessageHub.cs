﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace DotnetCoreSignalR.Hubs
{
    public class MessageHub : Hub
    {
        public async Task Send(string value) => await Clients.All.SendAsync("Send", value);
    }
}
